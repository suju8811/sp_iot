const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB();
const stripe = require('stripe')('sk_test_51HoJ7lEW8ELLg74EtYSjzjvLfCf1FMaHYA4uWmIrXknf7Z9zGz6ntGuogHcvZBfUQy2JJCRcd1MfipHccxhNfjzf00fdPvmEz8');

exports.handler = async (event) => {
  const { body } = event;
  
  try {
    const { stripeToken, currency, userid, courseid, amount, paymenttime} = JSON.parse(body);
    if (!stripeToken || !currency || !userid || !courseid || !amount || !paymenttime)
	{
      return {
        statusCode: 403,
        body: 'Mandatory fields Missing!'
      }
    }
    
    try{
		// Token is created using Stripe Checkout or Elements!
		const charge = await stripe.charges.create({
		  amount: parseInt(amount),
		  currency: currency,
		  description: 'IoT Course',
		  source: stripeToken,
		});
    }catch (err) {
       return {
         statusCode: 500,
         body: 'Error: ' + err,
       }
    }
    
    console.info('Saving Payment ==> UserId:' + userid + ', CourseId:' + courseid + ', Amount:' + amount + ', PaymentTime:' + paymenttime);
    await dynamodb.putItem({
      TableName: 'Payments',
      Item: {
      	userid: { S: userid },
      	courseid: { S: courseid },
        amount: { S: amount },
        paymenttime: { S: paymenttime }
      }
    }).promise();
    return {
       statusCode: 200,
       body: 'Payment Saved Successfully!',
    }
  } catch (err) {
    return {
       statusCode: 500,
       body: 'Error: ' + err,
    }
  }
};
