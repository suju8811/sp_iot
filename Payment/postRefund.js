const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB();

exports.handler = async (event) => {
  const { body } = event;
  try {
    const { userid, courseid, amount, refundtime} = JSON.parse(body);
    if (!userid || !courseid || !amount || !refundtime)
	{
      return {
        statusCode: 403,
        body: 'Mandatory fields Missing!'
      }
    }

    console.info('Saving refund ==> UserId:' + userid + ', CourseId:' + courseid + ', Amount:' + amount + ', RefundTime:' + refundtime);
    await dynamodb.putItem({
      TableName: 'Refunds',
      Item: {
      	userid: { S: userid},
      	courseid: { S: courseid },
        amount: { S: amount },
        refundtime: { S: refundtime }
      }
    }).promise();
    return {
       statusCode: 200,
       body: 'Refund Saved Successfully!',
    }
  } catch (err) {
    return {
       statusCode: 500,
       body: 'Error: ' + err,
    }
  }
};
